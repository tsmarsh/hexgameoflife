(ns hexgameoflife.core-test
  (:require [clojure.test :refer :all]
            [hexgameoflife.core :refer :all]))

(def game {})

(deftest a-cell-can-live
  (testing "a cell can live"
    (is (= false (is-alive? game 0 0)))
    (let [new-game (bring-to-life game 0 0)]
      (is (= true (is-alive? new-game 0 0)))))
  (testing "many cells can live"
    (let [new-game (bring-to-life game [[0 0] [0 1] [0 2]])]
      (is (= true (is-alive? new-game 0 0)))
      (is (= true (is-alive? new-game 0 1)))
      (is (= true (is-alive? new-game 0 2))))))

(deftest a-cell-can-die
  (testing "a cell can die"
    (is (= false (is-alive?
                   (kill
                     (bring-to-life game 0 0)
                     0 0)
                   0 0)))))

(deftest birth
  (testing "a dead cell with three neighbours comes to life"
    (let [new-game (bring-to-life game [[-1 0] [1 0] [-1 1]])
          iterated-game (evaluate new-game)]
          (is (= true (is-alive? iterated-game 0 0))))))

(deftest survival
  (testing "a live cell with two neighbours survives"
    (let [new-game (bring-to-life game [[-1 0] [0 0] [-1 1]])
          iterated-game (evaluate new-game)]
      (is (= true (is-alive? iterated-game 0 0)))))

  (testing "a dead cell with two neighbours dies"
    (let [new-game (bring-to-life game [[-1 0] [-1 1]])
          iterated-game (evaluate new-game)]
      (is (= false (is-alive? iterated-game 0 0))))))

(deftest lonliness
  (testing "a live cell with one neighbour dies from lonliness"
    (let [new-game (bring-to-life game [[-1 0] [0 0]])
          iterated-game (evaluate new-game)]
      (is (= false (is-alive? iterated-game 0 0)))))

  (testing "a live cell with no neighbours dies from lonliness"
    (let [new-game (bring-to-life game [[0 0]])
          iterated-game (evaluate new-game)]
      (is (= false (is-alive? iterated-game 0 0))))))

(deftest overcrowding
  (testing "a live cell with 3 neighbours dies"
    (let [new-game (bring-to-life game [[1  0] [1 -1] [0 -1]
                                        [0 0]])
          iterated-game (evaluate new-game)]
      (is (= false (is-alive? iterated-game 0 0)))))
  (testing "a live cell with 4 neighbours dies"
    (let [new-game (bring-to-life game [[1  0] [1 -1] [0 -1]
                                        [-1  0]
                                        [0 0]])
          iterated-game (evaluate new-game)]
      (is (= false (is-alive? iterated-game 0 0)))))

  (testing "a live cell with 5 neighbours dies"
    (let [new-game (bring-to-life game [[1  0] [1 -1] [0 -1]
                                        [-1  0] [-1 1]
                                        [0 0]])
          iterated-game (evaluate new-game)]
      (is (= false (is-alive? iterated-game 0 0)))))

  (testing "a live cell with 6 neighbours dies"
    (let [new-game (bring-to-life game [[1  0] [1 -1] [0 -1]
                                        [-1  0] [-1 1] [0 1]
                                        [0 0]])
          iterated-game (evaluate new-game)]
      (is (= false (is-alive? iterated-game 0 0))))))
