(ns hexgameoflife.core
  (:require [clojure.core.reducers :as r]
           [hexgameoflife.fold-helpers :as fr]
           [hexgameoflife.hex-grid :as h])
  (:gen-class))

(set! *warn-on-reflection* true)

(defn is-alive?
  [game q r]
  (contains? game [q r]))

(defn bring-to-life
  ([game q r]
    (assoc game [q r] :alive))
  ([game coords]
    (reduce #(apply bring-to-life % %2) game coords)))

(defn kill
  [game q r]
  (dissoc game [q r]))

(defn interesting-coords
  [game]
  (r/fold fr/combine-set
          (fn [coords cell]
            (apply conj coords (h/list-neighbours cell)))
          (keys game)))

(defn living-neighbours
  [game cell]
  (filter #(apply is-alive? game %) (h/list-neighbours cell)))

(defn evaluate-cell
  [game cell]
  (let [num-living-neigbours (count (living-neighbours game cell))
        alive? (apply is-alive? game cell)]
    (or (and (= 2 num-living-neigbours) alive?)
        (and (= 3 num-living-neigbours) (not alive?)))))

(defn evaluate
  [game]
  (r/fold
    fr/combine-map
    (fn [new cell]
      (if (evaluate-cell game cell)
        (bring-to-life new (first cell) (second cell))
        new))
    (interesting-coords game)))

