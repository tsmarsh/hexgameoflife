(ns hexgameoflife.fold-helpers
  (:require [clojure.set :as s]))

(defn combine-map
  ([] {})
  ([x y] (merge-with + x y)))

(defn combine-set
  ([] #{})
  ([x y] (s/union x y)))